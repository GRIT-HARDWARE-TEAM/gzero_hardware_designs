# G0_Hardware_Designs

This repo highlights some of the work that has been conducted by the GRIT Systems Hardware team towards the **G0 Smart Meter.**

For now, the major projects have been the;
* G0 Test board
* Switched Mode Power Supply Circuits
* FRAM test circuit.
* Customer Interface Unit (CIU)

Other projects can be found in their respective **Repos**