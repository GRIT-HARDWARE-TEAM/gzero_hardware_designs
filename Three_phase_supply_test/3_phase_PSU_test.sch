<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="no" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="no" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="no" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="silktop" color="7" fill="1" visible="no" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="no" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="adafruit">
<packages>
<package name="1X2-3.5MM">
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="-3.4" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<wire x1="3.6" y1="-2.2" x2="3.6" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<pad name="1" x="1.8" y="0" drill="1" diameter="2.1844"/>
<pad name="2" x="-1.7" y="0" drill="1" diameter="2.1844"/>
<text x="3" y="5" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="1X2">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="2.54" visible="pin" length="middle" direction="pas"/>
<pin name="2" x="-5.08" y="0" visible="pin" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1X2" prefix="J">
<description>3.5mm Terminal block
&lt;p&gt;http://www.ladyada.net/library/pcb/eaglelibrary.html&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="1X2" x="0" y="0"/>
</gates>
<devices>
<device name="-3.5MM" package="1X2-3.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fuse" urn="urn:adsk.eagle:library:233">
<description>&lt;b&gt;Fuses and Fuse Holders&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SH22,5" urn="urn:adsk.eagle:footprint:14030/1" library_version="1">
<description>&lt;b&gt;FUSE HOLDER&lt;/b&gt;&lt;p&gt; grid 22,5 mm, OGN0031 8201 Schurter (Buerklin)</description>
<wire x1="-12.573" y1="-3.937" x2="12.573" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="12.573" y1="3.937" x2="-12.573" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="5.08" y2="2.921" width="0.0508" layer="21"/>
<wire x1="-10.795" y1="3.683" x2="-5.715" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.683" x2="-10.795" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="3.683" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.683" x2="-10.795" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.683" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.683" x2="-11.43" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-3.683" x2="-11.43" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-11.176" y1="-2.032" x2="-11.176" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-11.176" y1="-3.175" x2="-10.795" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.175" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.715" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-2.032" x2="-11.43" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-1.27" x2="-11.176" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.176" y1="-1.27" x2="-11.176" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-3.683" x2="5.715" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.683" x2="10.795" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-3.683" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.683" x2="10.795" y2="3.683" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.683" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.683" x2="11.43" y2="3.683" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.683" x2="11.43" y2="2.032" width="0.1524" layer="21"/>
<wire x1="11.176" y1="2.032" x2="11.176" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.175" x2="10.795" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.175" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="5.715" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="11.43" y1="2.032" x2="11.43" y2="1.27" width="0.1524" layer="51"/>
<wire x1="11.43" y1="1.27" x2="11.176" y2="1.27" width="0.1524" layer="51"/>
<wire x1="11.176" y1="1.27" x2="11.176" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-5.207" y1="0" x2="5.207" y2="0" width="0.0508" layer="21"/>
<wire x1="12.573" y1="3.937" x2="12.573" y2="1.651" width="0.1524" layer="21"/>
<wire x1="12.573" y1="-1.651" x2="12.573" y2="1.651" width="0.1524" layer="51"/>
<wire x1="12.573" y1="-1.651" x2="12.573" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-12.573" y1="3.937" x2="-12.573" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-12.573" y1="-1.651" x2="-12.573" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-12.573" y1="-1.651" x2="-12.573" y2="-3.937" width="0.1524" layer="21"/>
<pad name="1" x="-11.2522" y="0" drill="1.3208" diameter="2.54" shape="long" rot="R90"/>
<pad name="2" x="11.2522" y="0" drill="1.3208" diameter="2.54" shape="long" rot="R90"/>
<text x="-12.7" y="-6.096" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="1.143" y="-6.096" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.541" y1="-2.032" x2="-10.16" y2="2.032" layer="51"/>
<rectangle x1="-10.541" y1="1.905" x2="-9.652" y2="2.032" layer="51"/>
<rectangle x1="-10.541" y1="-2.032" x2="-9.652" y2="-1.905" layer="51"/>
<rectangle x1="-5.715" y1="-3.175" x2="-5.08" y2="-2.032" layer="21"/>
<rectangle x1="-5.715" y1="2.032" x2="-5.08" y2="3.175" layer="21"/>
<rectangle x1="-5.715" y1="-2.032" x2="-5.08" y2="2.032" layer="21"/>
<rectangle x1="10.16" y1="-2.032" x2="10.541" y2="2.032" layer="51"/>
<rectangle x1="9.652" y1="-2.032" x2="10.541" y2="-1.905" layer="51"/>
<rectangle x1="9.652" y1="1.905" x2="10.541" y2="2.032" layer="51"/>
<rectangle x1="5.08" y1="2.032" x2="5.715" y2="3.175" layer="21"/>
<rectangle x1="5.08" y1="-3.175" x2="5.715" y2="-2.032" layer="21"/>
<rectangle x1="5.08" y1="-2.032" x2="5.715" y2="2.032" layer="21"/>
<rectangle x1="-5.08" y1="2.159" x2="5.08" y2="2.667" layer="21"/>
<rectangle x1="-5.08" y1="-2.667" x2="5.08" y2="-2.159" layer="21"/>
<rectangle x1="5.715" y1="-2.032" x2="9.652" y2="-1.905" layer="21"/>
<rectangle x1="5.715" y1="1.905" x2="9.652" y2="2.032" layer="21"/>
<rectangle x1="-9.652" y1="1.905" x2="-5.715" y2="2.032" layer="21"/>
<rectangle x1="-9.652" y1="-2.032" x2="-5.715" y2="-1.905" layer="21"/>
<rectangle x1="-10.541" y1="-2.54" x2="-5.715" y2="-2.032" layer="21"/>
<rectangle x1="-10.541" y1="2.032" x2="-5.715" y2="2.54" layer="21"/>
<rectangle x1="5.715" y1="2.032" x2="10.541" y2="2.54" layer="21"/>
<rectangle x1="5.715" y1="-2.54" x2="10.541" y2="-2.032" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="SH22,5" urn="urn:adsk.eagle:package:14059/1" type="box" library_version="1">
<description>FUSE HOLDER grid 22,5 mm, OGN0031 8201 Schurter (Buerklin)</description>
<packageinstances>
<packageinstance name="SH22,5"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="FUSE" urn="urn:adsk.eagle:symbol:14027/1" library_version="1">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.397" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SH22,5" urn="urn:adsk.eagle:component:14084/1" prefix="F" uservalue="yes" library_version="1">
<description>&lt;b&gt;FUSE HOLDER&lt;/b&gt;&lt;p&gt; grid 22,5 mm, OGN0031 8201, Schurter (Buerklin)</description>
<gates>
<gate name="1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SH22,5">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14059/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="SCHURTER" constant="no"/>
<attribute name="MPN" value="0031.8201" constant="no"/>
<attribute name="OC_FARNELL" value="1162740" constant="no"/>
<attribute name="OC_NEWARK" value="96F8317" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="varistor" urn="urn:adsk.eagle:library:410">
<description>&lt;b&gt;Varistors/Thermistors&lt;/b&gt;&lt;p&gt;
Block, Siemens and generic&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="S10K175" urn="urn:adsk.eagle:footprint:30458/1" library_version="1">
<description>&lt;b&gt;VARISTOR&lt;/b&gt;</description>
<wire x1="-1.397" y1="6.35" x2="1.397" y2="6.35" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="5.08" width="0.0508" layer="21"/>
<wire x1="1.524" y1="-5.08" x2="1.524" y2="0" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.54" x2="1.143" y2="0" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0" x2="-1.143" y2="2.54" width="0.0508" layer="21"/>
<wire x1="2.667" y1="-1.905" x2="2.667" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.905" x2="-2.667" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-2.667" x2="-2.032" y2="-4.953" width="0.1524" layer="51"/>
<wire x1="2.032" y1="2.667" x2="2.032" y2="4.953" width="0.1524" layer="51"/>
<wire x1="-2.667" y1="1.905" x2="-2.032" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.667" y1="-3.683" x2="-2.032" y2="-4.318" width="0.1524" layer="51" curve="90"/>
<wire x1="2.032" y1="4.318" x2="2.667" y2="3.683" width="0.1524" layer="51" curve="-90"/>
<wire x1="2.032" y1="-2.54" x2="2.667" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="2.032" y1="-5.715" x2="2.032" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-2.667" x2="-2.032" y2="5.715" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-6.35" x2="2.032" y2="-5.715" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.032" y1="-5.715" x2="-1.397" y2="-6.35" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.032" y1="5.715" x2="-1.397" y2="6.35" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.397" y1="6.35" x2="2.032" y2="5.715" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="-6.35" x2="1.397" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="2.032" y1="4.953" x2="2.032" y2="5.715" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-5.715" x2="-2.032" y2="-4.953" width="0.1524" layer="21"/>
<pad name="1" x="-1.143" y="-3.81" drill="1.016" shape="long"/>
<pad name="2" x="1.143" y="3.81" drill="1.016" shape="long"/>
<text x="-3.048" y="-5.08" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.048" y="0.762" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="S10K175" urn="urn:adsk.eagle:package:30555/1" type="box" library_version="1">
<description>VARISTOR</description>
<packageinstances>
<packageinstance name="S10K175"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="VDR" urn="urn:adsk.eagle:symbol:30423/1" library_version="1">
<wire x1="-3.81" y1="-0.889" x2="3.81" y2="-0.889" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.889" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.889" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.1938" y1="-2.1082" x2="-1.1938" y2="-3.4798" width="0.1524" layer="94"/>
<wire x1="-0.9398" y1="-3.7338" x2="-1.1938" y2="-3.4798" width="0.1524" layer="94"/>
<wire x1="-0.9398" y1="-3.7338" x2="-0.381" y2="-3.7338" width="0.1524" layer="94"/>
<wire x1="-0.127" y1="-3.4798" x2="-0.381" y2="-3.7338" width="0.1524" layer="94"/>
<wire x1="-0.127" y1="-3.4798" x2="-0.127" y2="-2.1082" width="0.1524" layer="94"/>
<wire x1="5.715" y1="5.08" x2="5.715" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.715" y1="2.54" x2="6.096" y2="3.81" width="0.1524" layer="94"/>
<wire x1="6.096" y1="3.81" x2="5.334" y2="3.81" width="0.1524" layer="94"/>
<wire x1="5.334" y1="3.81" x2="5.715" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.445" y1="2.54" x2="4.445" y2="5.08" width="0.1524" layer="94"/>
<wire x1="4.445" y1="5.08" x2="4.826" y2="3.81" width="0.1524" layer="94"/>
<wire x1="4.826" y1="3.81" x2="4.064" y2="3.81" width="0.1524" layer="94"/>
<wire x1="4.064" y1="3.81" x2="4.445" y2="5.08" width="0.1524" layer="94"/>
<text x="1.27" y="-3.7084" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-5.715" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="S10K175" urn="urn:adsk.eagle:component:30643/1" prefix="R" library_version="1">
<description>&lt;b&gt;VARISTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VDR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="S10K175">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30555/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rectifier" urn="urn:adsk.eagle:library:336">
<description>&lt;b&gt;Rectifiers&lt;/b&gt;&lt;p&gt;
General Instrument, Semikron, Diotec, Fagor&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DFS" urn="urn:adsk.eagle:footprint:23863/1" library_version="1">
<description>&lt;b&gt;Surface Mount Glass Passivated Bridge Rectifiers&lt;/hb&gt;&lt;p&gt;
Source: Comchip Bridge Rectifiers DF005S-G thru DF10S-G.pdf</description>
<wire x1="-4.05" y1="3.1" x2="4.05" y2="3.1" width="0.254" layer="21"/>
<wire x1="4.05" y1="3.1" x2="4.05" y2="-3.1" width="0.254" layer="21"/>
<wire x1="4.05" y1="-3.1" x2="-4.05" y2="-3.1" width="0.254" layer="21"/>
<wire x1="-4.05" y1="-3.1" x2="-4.05" y2="3.1" width="0.254" layer="21"/>
<wire x1="-2.55" y1="-1.675" x2="-2" y2="-1.675" width="0.1016" layer="21"/>
<wire x1="-2.275" y1="-1.925" x2="-2.275" y2="-1.4" width="0.1016" layer="21"/>
<wire x1="1.99" y1="-1.675" x2="2.54" y2="-1.675" width="0.1016" layer="21"/>
<wire x1="-2.25" y1="1.75" x2="-2.625" y2="1.75" width="0.1016" layer="21" curve="172.37185"/>
<wire x1="-1.875" y1="1.75" x2="-2.25" y2="1.75" width="0.1016" layer="21" curve="-172.37185"/>
<wire x1="2.25" y1="1.75" x2="2.625" y2="1.75" width="0.1016" layer="21" curve="172.37185"/>
<wire x1="1.875" y1="1.75" x2="2.25" y2="1.75" width="0.1016" layer="21" curve="-172.37185"/>
<smd name="+" x="-2.55" y="-4.5" dx="1.5" dy="1.7" layer="1"/>
<smd name="-" x="2.55" y="-4.5" dx="1.5" dy="1.7" layer="1"/>
<smd name="AC2" x="2.55" y="4.5" dx="1.5" dy="1.7" layer="1" rot="R180"/>
<smd name="AC1" x="-2.55" y="4.5" dx="1.5" dy="1.7" layer="1" rot="R180"/>
<text x="-4.6" y="-3.225" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.425" y="-3.3" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3.2" y1="-5.1" x2="-1.9" y2="-3.2" layer="51"/>
<rectangle x1="1.9" y1="-5.1" x2="3.2" y2="-3.2" layer="51"/>
<rectangle x1="1.9" y1="3.2" x2="3.2" y2="5.1" layer="51" rot="R180"/>
<rectangle x1="-3.2" y1="3.2" x2="-1.9" y2="5.1" layer="51" rot="R180"/>
</package>
</packages>
<packages3d>
<package3d name="DFS" urn="urn:adsk.eagle:package:23903/1" type="box" library_version="1">
<description>Surface Mount Glass Passivated Bridge Rectifiers
Source: Comchip Bridge Rectifiers DF005S-G thru DF10S-G.pdf</description>
<packageinstances>
<packageinstance name="DFS"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DB" urn="urn:adsk.eagle:symbol:23823/1" library_version="1">
<wire x1="-1.905" y1="-3.175" x2="-4.064" y2="-2.794" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-3.175" x2="-2.286" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.7178" y1="-4.0386" x2="-0.9398" y2="-2.2606" width="0.254" layer="94"/>
<wire x1="-1.905" y1="3.175" x2="-4.064" y2="2.794" width="0.254" layer="94"/>
<wire x1="-1.905" y1="3.175" x2="-2.286" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.7178" y1="4.0386" x2="-1.0668" y2="2.3876" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="2.794" y2="4.064" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="1.016" y2="2.286" width="0.254" layer="94"/>
<wire x1="2.3622" y1="1.016" x2="4.1402" y2="2.794" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.905" x2="2.794" y2="-4.064" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.905" x2="1.016" y2="-2.286" width="0.254" layer="94"/>
<wire x1="2.3622" y1="-1.0668" x2="4.1402" y2="-2.8448" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="3.175" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="-1.905" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-3.2766" y1="-1.8034" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="-2.794" x2="-2.286" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.2766" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="2.794" x2="-2.286" y2="1.016" width="0.254" layer="94"/>
<wire x1="-1.905" y1="3.175" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="1.8034" y2="3.2766" width="0.1524" layer="94"/>
<wire x1="1.016" y1="2.286" x2="2.794" y2="4.064" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.8034" y2="-3.2766" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.286" x2="2.794" y2="-4.064" width="0.254" layer="94"/>
<text x="5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="AC1" x="0" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="+" x="5.08" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<pin name="AC2" x="0" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="-" x="-5.08" y="0" visible="off" length="point" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DF*S" urn="urn:adsk.eagle:component:23938/1" prefix="B" library_version="1">
<description>&lt;b&gt;Surface Mount Glass Passivated Bridge Rectifiers&lt;/b&gt;&lt;p&gt;
Source: Source: Comchip Bridge Rectifiers DF005S-G thru DF10S-G.pdf</description>
<gates>
<gate name="G$1" symbol="DB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFS">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
<connect gate="G$1" pin="AC1" pad="AC1"/>
<connect gate="G$1" pin="AC2" pad="AC2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23903/1"/>
</package3dinstances>
<technologies>
<technology name="005"/>
<technology name="01"/>
<technology name="02"/>
<technology name="04"/>
<technology name="06"/>
<technology name="08"/>
<technology name="10"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="discrete" urn="urn:adsk.eagle:library:211">
<description>Discrete devices (Antenna, Arrrester, Thermistor)</description>
<packages>
<package name="RS-2,5" urn="urn:adsk.eagle:footprint:12921/1" library_version="1">
<description>&lt;b&gt;Resistor&lt;/b&gt;</description>
<wire x1="-0.381" y1="0" x2="0.381" y2="0" width="0.127" layer="21"/>
<circle x="-1.27" y="0" radius="1.27" width="0.127" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.54" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="R-7,5" urn="urn:adsk.eagle:footprint:12922/1" library_version="1">
<description>&lt;b&gt;Resistor&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="3.048" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-3.048" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R-5" urn="urn:adsk.eagle:footprint:12923/1" library_version="1">
<description>&lt;b&gt;Resistor&lt;/b&gt;</description>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.27" y1="0" x2="1.778" y2="0" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="-1.778" y1="0" x2="-1.27" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="0" width="0.127" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.635" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="RS-2,5" urn="urn:adsk.eagle:package:12929/1" type="box" library_version="1">
<description>Resistor</description>
<packageinstances>
<packageinstance name="RS-2,5"/>
</packageinstances>
</package3d>
<package3d name="R-7,5" urn="urn:adsk.eagle:package:12930/1" type="box" library_version="1">
<description>Resistor</description>
<packageinstances>
<packageinstance name="R-7,5"/>
</packageinstances>
</package3d>
<package3d name="R-5" urn="urn:adsk.eagle:package:12931/1" type="box" library_version="1">
<description>Resistor</description>
<packageinstances>
<packageinstance name="R-5"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="THERMIST" urn="urn:adsk.eagle:symbol:12920/1" library_version="1">
<wire x1="2.54" y1="1.016" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.016" x2="-2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.016" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="3.1496" y1="-2.032" x2="2.032" y2="-2.032" width="0.254" layer="94"/>
<wire x1="2.032" y1="-2.032" x2="-2.032" y2="2.032" width="0.254" layer="94"/>
<text x="-2.54" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="THERMISTOR_" urn="urn:adsk.eagle:component:12937/1" prefix="TR" uservalue="yes" library_version="1">
<description>&lt;b&gt;Thermo Resistor&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="THERMIST" x="0" y="0"/>
</gates>
<devices>
<device name="R-2,5" package="RS-2,5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12929/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R-7,5" package="R-7,5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12930/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R-5" package="R-5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12931/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SRP1265A-330M">
<packages>
<package name="IND_SRP1265A-330M">
<text x="-5.847809375" y="7.286309375" size="1.200009375" layer="25">&gt;NAME</text>
<text x="-5.8454" y="-7.755509375" size="1.20008125" layer="27">&gt;VALUE</text>
<wire x1="-6.25" y1="6.25" x2="6.25" y2="6.25" width="0.127" layer="51"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="6.25" y1="-6.25" x2="-6.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-6.25" y1="-6.25" x2="-6.25" y2="6.25" width="0.127" layer="51"/>
<wire x1="-6.25" y1="6.25" x2="6.25" y2="6.25" width="0.127" layer="21"/>
<wire x1="-6.25" y1="-6.25" x2="-6.25" y2="-3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3" x2="6.25" y2="-6.25" width="0.127" layer="21"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="3" width="0.127" layer="21"/>
<wire x1="-6.25" y1="3" x2="-6.25" y2="6.25" width="0.127" layer="21"/>
<wire x1="6.25" y1="-6.25" x2="-6.25" y2="-6.25" width="0.127" layer="21"/>
<wire x1="-6.5" y1="6.5" x2="6.5" y2="6.5" width="0.05" layer="39"/>
<wire x1="6.5" y1="6.5" x2="6.5" y2="2.75" width="0.05" layer="39"/>
<wire x1="6.5" y1="2.75" x2="7.35" y2="2.75" width="0.05" layer="39"/>
<wire x1="7.35" y1="2.75" x2="7.35" y2="-2.75" width="0.05" layer="39"/>
<wire x1="7.35" y1="-2.75" x2="6.5" y2="-2.75" width="0.05" layer="39"/>
<wire x1="6.5" y1="-2.75" x2="6.5" y2="-6.5" width="0.05" layer="39"/>
<wire x1="6.5" y1="-6.5" x2="-6.5" y2="-6.5" width="0.05" layer="39"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="-2.75" width="0.05" layer="39"/>
<wire x1="-6.5" y1="-2.75" x2="-7.35" y2="-2.75" width="0.05" layer="39"/>
<wire x1="-7.35" y1="-2.75" x2="-7.35" y2="2.75" width="0.05" layer="39"/>
<wire x1="-7.35" y1="2.75" x2="-6.5" y2="2.75" width="0.05" layer="39"/>
<wire x1="-6.5" y1="2.75" x2="-6.5" y2="6.5" width="0.05" layer="39"/>
<circle x="-7.62" y="0" radius="0.1" width="0.2" layer="21"/>
<smd name="1" x="-5.55" y="0" dx="3.1" dy="5" layer="1"/>
<smd name="2" x="5.55" y="0" dx="3.1" dy="5" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="SRP1265A-330M">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94" curve="-191.421"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.1524" layer="94" curve="-191.421"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.1524" layer="94" curve="-191.421"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.1524" layer="94" curve="-191.421"/>
<text x="-5.099959375" y="1.912490625" size="1.784990625" layer="95">&gt;NAME</text>
<text x="-5.095790625" y="-2.5479" size="1.783540625" layer="96">&gt;VALUE</text>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SRP1265A-330M" prefix="L">
<description>Inductor Power Shielded Wirewound 33uH 20% 100KHz Carbonyl Powder 8A 58mOhm DCR Automotive T/R</description>
<gates>
<gate name="G$1" symbol="SRP1265A-330M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IND_SRP1265A-330M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Inductor Power Shielded Wirewound 33uH 20% 100KHz Carbonyl Powder 8A 58mOhm DCR Automotive T/R "/>
<attribute name="MF" value="Bourns"/>
<attribute name="MP" value="SRP1265A-330M"/>
<attribute name="PACKAGE" value="1008 Bourns"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
<clearance class="0" value="0.205"/>
</class>
<class number="3" name="Line" width="0.762" drill="0.508">
<clearance class="3" value="0.381"/>
</class>
<class number="4" name="Neutral" width="0.762" drill="0.508">
<clearance class="4" value="0.381"/>
</class>
</classes>
<parts>
<part name="F1" library="fuse" library_urn="urn:adsk.eagle:library:233" deviceset="SH22,5" device="" package3d_urn="urn:adsk.eagle:package:14059/1" value="FW20A10R0JA"/>
<part name="G2" library="rectifier" library_urn="urn:adsk.eagle:library:336" deviceset="DF*S" device="" package3d_urn="urn:adsk.eagle:package:23903/1" technology="10" value="TBS22J-TP"/>
<part name="TR3" library="discrete" library_urn="urn:adsk.eagle:library:211" deviceset="THERMISTOR_" device="R-7,5" package3d_urn="urn:adsk.eagle:package:12930/1" value="10R-NTC"/>
<part name="MOV2" library="varistor" library_urn="urn:adsk.eagle:library:410" deviceset="S10K175" device="" package3d_urn="urn:adsk.eagle:package:30555/1" value="MOV 250 V (AC)">
<attribute name="MF" value="LITTELFUSE"/>
<attribute name="MPN" value="V250LA10P"/>
<attribute name="OC_NEWARK" value="58K7410"/>
</part>
<part name="F2" library="fuse" library_urn="urn:adsk.eagle:library:233" deviceset="SH22,5" device="" package3d_urn="urn:adsk.eagle:package:14059/1" value="FW20A10R0JA"/>
<part name="G1" library="rectifier" library_urn="urn:adsk.eagle:library:336" deviceset="DF*S" device="" package3d_urn="urn:adsk.eagle:package:23903/1" technology="10" value="TBS22J-TP"/>
<part name="TR4" library="discrete" library_urn="urn:adsk.eagle:library:211" deviceset="THERMISTOR_" device="R-7,5" package3d_urn="urn:adsk.eagle:package:12930/1" value="10R-NTC"/>
<part name="MOV6" library="varistor" library_urn="urn:adsk.eagle:library:410" deviceset="S10K175" device="" package3d_urn="urn:adsk.eagle:package:30555/1" value="MOV 250 V (AC)">
<attribute name="MF" value="LITTELFUSE"/>
<attribute name="MPN" value="V250LA10P"/>
<attribute name="OC_NEWARK" value="58K7410"/>
</part>
<part name="F3" library="fuse" library_urn="urn:adsk.eagle:library:233" deviceset="SH22,5" device="" package3d_urn="urn:adsk.eagle:package:14059/1" value="FW20A10R0JA"/>
<part name="G4" library="rectifier" library_urn="urn:adsk.eagle:library:336" deviceset="DF*S" device="" package3d_urn="urn:adsk.eagle:package:23903/1" technology="10" value="TBS22J-TP"/>
<part name="TR6" library="discrete" library_urn="urn:adsk.eagle:library:211" deviceset="THERMISTOR_" device="R-7,5" package3d_urn="urn:adsk.eagle:package:12930/1" value="10R-NTC"/>
<part name="MOV7" library="varistor" library_urn="urn:adsk.eagle:library:410" deviceset="S10K175" device="" package3d_urn="urn:adsk.eagle:package:30555/1" value="MOV 250 V (AC)">
<attribute name="MF" value="LITTELFUSE"/>
<attribute name="MPN" value="V250LA10P"/>
<attribute name="OC_NEWARK" value="58K7410"/>
</part>
<part name="J1" library="adafruit" deviceset="1X2" device="-3.5MM"/>
<part name="J2" library="adafruit" deviceset="1X2" device="-3.5MM"/>
<part name="J3" library="adafruit" deviceset="1X2" device="-3.5MM"/>
<part name="L1" library="SRP1265A-330M" deviceset="SRP1265A-330M" device="" value="47uH"/>
<part name="J4" library="adafruit" deviceset="1X2" device="-3.5MM"/>
<part name="J5" library="adafruit" deviceset="1X2" device="-3.5MM"/>
<part name="F4" library="fuse" library_urn="urn:adsk.eagle:library:233" deviceset="SH22,5" device="" package3d_urn="urn:adsk.eagle:package:14059/1" value="FW20A10R0JA"/>
<part name="TR1" library="discrete" library_urn="urn:adsk.eagle:library:211" deviceset="THERMISTOR_" device="R-7,5" package3d_urn="urn:adsk.eagle:package:12930/1" value="10R-NTC"/>
<part name="MOV1" library="varistor" library_urn="urn:adsk.eagle:library:410" deviceset="S10K175" device="" package3d_urn="urn:adsk.eagle:package:30555/1" value="MOV 250 V (AC)">
<attribute name="MF" value="LITTELFUSE"/>
<attribute name="MPN" value="V250LA10P"/>
<attribute name="OC_NEWARK" value="58K7410"/>
</part>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="F1" gate="1" x="0" y="213.36"/>
<instance part="G2" gate="G$1" x="53.34" y="213.36" rot="R90"/>
<instance part="TR3" gate="G$1" x="17.78" y="213.36"/>
<instance part="MOV2" gate="G$1" x="25.4" y="203.2" smashed="yes" rot="R270">
<attribute name="NAME" x="27.1526" y="207.01" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="22.225" y="195.834" size="1.778" layer="96" rot="R90"/>
<attribute name="OC_NEWARK" x="25.4" y="203.2" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MF" x="25.4" y="203.2" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MPN" x="25.4" y="203.2" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="F2" gate="1" x="0" y="251.46"/>
<instance part="G1" gate="G$1" x="53.34" y="251.46" rot="R90"/>
<instance part="TR4" gate="G$1" x="17.78" y="251.46"/>
<instance part="MOV6" gate="G$1" x="25.4" y="241.3" smashed="yes" rot="R270">
<attribute name="NAME" x="27.1526" y="245.11" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="22.225" y="233.934" size="1.778" layer="96" rot="R90"/>
<attribute name="OC_NEWARK" x="25.4" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MF" x="25.4" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MPN" x="25.4" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="F3" gate="1" x="0" y="175.26"/>
<instance part="G4" gate="G$1" x="53.34" y="175.26" rot="R90"/>
<instance part="TR6" gate="G$1" x="17.78" y="175.26"/>
<instance part="MOV7" gate="G$1" x="25.4" y="165.1" smashed="yes" rot="R270">
<attribute name="NAME" x="27.1526" y="168.91" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="22.225" y="157.734" size="1.778" layer="96" rot="R90"/>
<attribute name="OC_NEWARK" x="25.4" y="165.1" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MF" x="25.4" y="165.1" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MPN" x="25.4" y="165.1" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="J1" gate="G$1" x="-30.48" y="238.76"/>
<instance part="J2" gate="G$1" x="-30.48" y="200.66"/>
<instance part="J3" gate="G$1" x="-30.48" y="162.56"/>
<instance part="L1" gate="G$1" x="76.2" y="238.76"/>
<instance part="J4" gate="G$1" x="142.24" y="205.74"/>
<instance part="J5" gate="G$1" x="-30.48" y="121.92"/>
<instance part="F4" gate="1" x="0" y="132.08"/>
<instance part="TR1" gate="G$1" x="17.78" y="132.08"/>
<instance part="MOV1" gate="G$1" x="25.4" y="121.92" smashed="yes" rot="R270">
<attribute name="NAME" x="27.1526" y="125.73" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="22.225" y="114.554" size="1.778" layer="96" rot="R90"/>
<attribute name="OC_NEWARK" x="25.4" y="121.92" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MF" x="25.4" y="121.92" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MPN" x="25.4" y="121.92" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="PHASE_2" class="3">
<segment>
<pinref part="TR3" gate="G$1" pin="1"/>
<wire x1="22.86" y1="213.36" x2="25.4" y2="213.36" width="0.1524" layer="91"/>
<wire x1="25.4" y1="208.28" x2="25.4" y2="213.36" width="0.1524" layer="91"/>
<junction x="25.4" y="213.36"/>
<pinref part="MOV2" gate="G$1" pin="1"/>
<wire x1="25.4" y1="213.36" x2="35.56" y2="213.36" width="0.1524" layer="91"/>
<wire x1="35.56" y1="213.36" x2="35.56" y2="228.6" width="0.1524" layer="91"/>
<wire x1="35.56" y1="228.6" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
<wire x1="60.96" y1="228.6" x2="60.96" y2="251.46" width="0.1524" layer="91"/>
<pinref part="G1" gate="G$1" pin="AC2"/>
<wire x1="60.96" y1="251.46" x2="58.42" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HVDC" class="0">
<segment>
<pinref part="G1" gate="G$1" pin="+"/>
<wire x1="53.34" y1="256.54" x2="53.34" y2="261.62" width="0.1524" layer="91"/>
<wire x1="63.5" y1="261.62" x2="53.34" y2="261.62" width="0.1524" layer="91"/>
<wire x1="63.5" y1="218.44" x2="63.5" y2="238.76" width="0.1524" layer="91"/>
<wire x1="63.5" y1="238.76" x2="63.5" y2="261.62" width="0.1524" layer="91"/>
<wire x1="53.34" y1="218.44" x2="63.5" y2="218.44" width="0.1524" layer="91"/>
<pinref part="G2" gate="G$1" pin="+"/>
<pinref part="G4" gate="G$1" pin="+"/>
<wire x1="53.34" y1="180.34" x2="63.5" y2="180.34" width="0.1524" layer="91"/>
<wire x1="63.5" y1="180.34" x2="63.5" y2="218.44" width="0.1524" layer="91"/>
<junction x="63.5" y="218.44"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="68.58" y1="238.76" x2="63.5" y2="238.76" width="0.1524" layer="91"/>
<junction x="63.5" y="238.76"/>
</segment>
</net>
<net name="GND/HV" class="0">
<segment>
<pinref part="G4" gate="G$1" pin="-"/>
<wire x1="53.34" y1="170.18" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="160.02" x2="53.34" y2="149.86" width="0.1524" layer="91"/>
<wire x1="53.34" y1="149.86" x2="88.9" y2="149.86" width="0.1524" layer="91"/>
<pinref part="G1" gate="G$1" pin="-"/>
<wire x1="53.34" y1="238.76" x2="53.34" y2="246.38" width="0.1524" layer="91"/>
<wire x1="43.18" y1="238.76" x2="53.34" y2="238.76" width="0.1524" layer="91"/>
<pinref part="G2" gate="G$1" pin="-"/>
<wire x1="53.34" y1="208.28" x2="53.34" y2="198.12" width="0.1524" layer="91"/>
<wire x1="53.34" y1="198.12" x2="43.18" y2="198.12" width="0.1524" layer="91"/>
<wire x1="43.18" y1="198.12" x2="43.18" y2="238.76" width="0.1524" layer="91"/>
<junction x="43.18" y="198.12"/>
<wire x1="43.18" y1="198.12" x2="43.18" y2="160.02" width="0.1524" layer="91"/>
<wire x1="43.18" y1="160.02" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<junction x="53.34" y="160.02"/>
<wire x1="88.9" y1="149.86" x2="111.76" y2="149.86" width="0.1524" layer="91"/>
<label x="111.76" y="149.86" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="129.54" y1="205.74" x2="137.16" y2="205.74" width="0.1524" layer="91"/>
<label x="129.54" y="205.74" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N" class="3">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="-43.18" y1="238.76" x2="-35.56" y2="238.76" width="0.1524" layer="91"/>
<label x="-43.18" y="238.76" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="-43.18" y1="200.66" x2="-35.56" y2="200.66" width="0.1524" layer="91"/>
<label x="-43.18" y="200.66" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="-43.18" y1="162.56" x2="-35.56" y2="162.56" width="0.1524" layer="91"/>
<label x="-43.18" y="162.56" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="-43.18" y1="121.92" x2="-35.56" y2="121.92" width="0.1524" layer="91"/>
<label x="-43.18" y="121.92" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="25.4" y1="198.12" x2="25.4" y2="190.5" width="0.1524" layer="91"/>
<pinref part="MOV2" gate="G$1" pin="2"/>
<label x="25.4" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MOV6" gate="G$1" pin="2"/>
<wire x1="25.4" y1="236.22" x2="25.4" y2="228.6" width="0.1524" layer="91"/>
<label x="25.4" y="228.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MOV7" gate="G$1" pin="2"/>
<wire x1="25.4" y1="160.02" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<label x="25.4" y="152.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MOV1" gate="G$1" pin="2"/>
<wire x1="25.4" y1="116.84" x2="25.4" y2="109.22" width="0.1524" layer="91"/>
<label x="25.4" y="109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="73.66" y="109.22" size="1.778" layer="95" xref="yes"/>
<pinref part="G4" gate="G$1" pin="AC1"/>
<wire x1="48.26" y1="175.26" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
<wire x1="73.66" y1="109.22" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="F1" gate="1" pin="2"/>
<pinref part="TR3" gate="G$1" pin="2"/>
<wire x1="5.08" y1="213.36" x2="10.16" y2="213.36" width="0.1524" layer="91"/>
<wire x1="10.16" y1="213.36" x2="12.7" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L3" class="0">
<segment>
<label x="-12.7" y="175.26" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-5.08" y1="175.26" x2="-12.7" y2="175.26" width="0.1524" layer="91"/>
<pinref part="F3" gate="1" pin="1"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="165.1" x2="-35.56" y2="165.1" width="0.1524" layer="91"/>
<label x="-43.18" y="165.1" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PHASE_1" class="3">
<segment>
<pinref part="TR4" gate="G$1" pin="1"/>
<wire x1="22.86" y1="251.46" x2="25.4" y2="251.46" width="0.1524" layer="91"/>
<wire x1="25.4" y1="251.46" x2="27.94" y2="251.46" width="0.1524" layer="91"/>
<wire x1="25.4" y1="246.38" x2="25.4" y2="251.46" width="0.1524" layer="91"/>
<junction x="25.4" y="251.46"/>
<pinref part="MOV6" gate="G$1" pin="1"/>
<pinref part="G1" gate="G$1" pin="AC1"/>
<wire x1="48.26" y1="251.46" x2="43.18" y2="251.46" width="0.1524" layer="91"/>
<wire x1="27.94" y1="251.46" x2="43.18" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="F2" gate="1" pin="2"/>
<pinref part="TR4" gate="G$1" pin="2"/>
<wire x1="5.08" y1="251.46" x2="10.16" y2="251.46" width="0.1524" layer="91"/>
<wire x1="10.16" y1="251.46" x2="12.7" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PHASE_3" class="3">
<segment>
<pinref part="TR6" gate="G$1" pin="1"/>
<wire x1="22.86" y1="175.26" x2="25.4" y2="175.26" width="0.1524" layer="91"/>
<wire x1="25.4" y1="175.26" x2="27.94" y2="175.26" width="0.1524" layer="91"/>
<wire x1="25.4" y1="170.18" x2="25.4" y2="175.26" width="0.1524" layer="91"/>
<junction x="25.4" y="175.26"/>
<pinref part="MOV7" gate="G$1" pin="1"/>
<wire x1="27.94" y1="175.26" x2="38.1" y2="175.26" width="0.1524" layer="91"/>
<wire x1="38.1" y1="175.26" x2="40.64" y2="175.26" width="0.1524" layer="91"/>
<wire x1="40.64" y1="175.26" x2="40.64" y2="213.36" width="0.1524" layer="91"/>
<pinref part="G2" gate="G$1" pin="AC1"/>
<wire x1="40.64" y1="213.36" x2="48.26" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="F3" gate="1" pin="2"/>
<pinref part="TR6" gate="G$1" pin="2"/>
<wire x1="5.08" y1="175.26" x2="12.7" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L1" class="0">
<segment>
<label x="-12.7" y="251.46" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-5.08" y1="251.46" x2="-12.7" y2="251.46" width="0.1524" layer="91"/>
<pinref part="F2" gate="1" pin="1"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="241.3" x2="-35.56" y2="241.3" width="0.1524" layer="91"/>
<label x="-43.18" y="241.3" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="L2" class="0">
<segment>
<label x="-12.7" y="213.36" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-5.08" y1="213.36" x2="-12.7" y2="213.36" width="0.1524" layer="91"/>
<pinref part="F1" gate="1" pin="1"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="203.2" x2="-35.56" y2="203.2" width="0.1524" layer="91"/>
<label x="-43.18" y="203.2" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RECTIFIED_OUTPUT" class="0">
<segment>
<wire x1="88.9" y1="238.76" x2="111.76" y2="238.76" width="0.1524" layer="91"/>
<label x="111.76" y="238.76" size="1.016" layer="95" xref="yes"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="83.82" y1="238.76" x2="88.9" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="129.54" y1="208.28" x2="137.16" y2="208.28" width="0.1524" layer="91"/>
<label x="129.54" y="208.28" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="L4" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="124.46" x2="-35.56" y2="124.46" width="0.1524" layer="91"/>
<label x="-43.18" y="124.46" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="F4" gate="1" pin="1"/>
<wire x1="-10.16" y1="132.08" x2="-5.08" y2="132.08" width="0.1524" layer="91"/>
<label x="-10.16" y="132.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="F4" gate="1" pin="2"/>
<pinref part="TR1" gate="G$1" pin="2"/>
<wire x1="5.08" y1="132.08" x2="12.7" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="TR1" gate="G$1" pin="1"/>
<wire x1="22.86" y1="132.08" x2="25.4" y2="132.08" width="0.1524" layer="91"/>
<wire x1="25.4" y1="132.08" x2="45.72" y2="132.08" width="0.1524" layer="91"/>
<wire x1="45.72" y1="132.08" x2="45.72" y2="193.04" width="0.1524" layer="91"/>
<pinref part="MOV1" gate="G$1" pin="1"/>
<wire x1="25.4" y1="127" x2="25.4" y2="132.08" width="0.1524" layer="91"/>
<junction x="25.4" y="132.08"/>
<wire x1="45.72" y1="193.04" x2="60.96" y2="193.04" width="0.1524" layer="91"/>
<wire x1="60.96" y1="193.04" x2="60.96" y2="213.36" width="0.1524" layer="91"/>
<pinref part="G2" gate="G$1" pin="AC2"/>
<wire x1="60.96" y1="213.36" x2="58.42" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
