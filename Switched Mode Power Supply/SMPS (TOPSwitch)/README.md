# Project Status

These circuits are currently not working. The TOPSwitch device seems to go into **auto-restart** whenever the board is switched on. 

**Probable cause:**
1. Some timing components such as resistors or capacitors, are delaying the current into one or more pins of the device, as such the device goes into auto-restart, in a matter of micro-seconds upon startup.
2. The layout of the board is not well suited for SMPS designs as there is no isolation distance between the primary and the secondary side.

