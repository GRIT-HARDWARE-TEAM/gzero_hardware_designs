# Project Status

The "PSU" circuit is not working. Probable causes are:
1. Damaged components on the board which need to be replaced.

    *Update:* Board now works, ensure to include resistor **R8**.
    It delivers about 400mA of current with regulated voltage of 4.5 to 5V.

*Recommendation:* A different transformer should be used to deliver more current.

The "Old board (Ikenna)", was working but had unregulated output voltages and increased operating temperature even at no load.

Probable causes:
1. A regulation circuit was not implemented.
2. The frequency of oscillation of the transformer is to be about 132kHz, but the Viper12A has a fixed switching frequency of 60kHz.