# Project Status

This SMPS uses a discrete MOSFET and an external PWM controller (UC3842).

This circuit does not work.

**Probable Cause:** Insufficient voltage on the controller's VDD pin.

*Recommendation:* A different transformer should be used.